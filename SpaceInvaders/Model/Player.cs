﻿namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Models a player that is displayed on the scoreboard.
    /// </summary>
    public class Player
    {
        #region Data members

        /// <summary>
        ///     The player name
        /// </summary>
        public string PlayerName;

        /// <summary>
        ///     The player score
        /// </summary>
        public int PlayerScore;

        /// <summary>
        ///     The level completed
        /// </summary>
        public int LevelCompleted;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Player" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="score">The score.</param>
        /// <param name="levelCompleted">The level completed.</param>
        public Player(string name, int score, int levelCompleted)
        {
            this.PlayerName = name;
            this.PlayerScore = score;
            this.LevelCompleted = levelCompleted;
        }

        #endregion
    }
}