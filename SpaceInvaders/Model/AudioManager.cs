﻿using System;
using Windows.ApplicationModel;
using Windows.Storage;
using Windows.UI.Xaml.Controls;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Manages the game sounds
    /// </summary>
    internal class AudioManager
    {
        #region Methods

        /// <summary>
        /// Plays the SFX.
        /// </summary>
        /// <param name="filename">The filename.</param>
        public async void PlaySfx(string filename)
        {
            var folder = Package.Current.InstalledLocation;
            folder = await folder.GetFolderAsync("Sound");
            var sfx = await folder.GetFileAsync(filename);

            var mediaPlayer = new MediaElement();
            mediaPlayer.SetSource(await sfx.OpenAsync(FileAccessMode.Read), sfx.ContentType);

            mediaPlayer.Play();
        }

        #endregion
    }
}