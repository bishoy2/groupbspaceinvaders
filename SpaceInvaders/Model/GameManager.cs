﻿using System;
using System.Collections.Generic;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Manages the entire game.
    /// </summary>
    public class GameManager
    {
        #region Data members

        private const double PlayerShipBottomOffset = 30;
        private const int BulletSpacingDelay = 100;
        private const int BonusShipSpaceYOffset = 50;
        private const int TickInterval = 5;

        private int difficultyModifier;
        private int maxPlayerBullets = 3;
        private int ticksToPlayerShip;
        private readonly int bonusShipChance = 3;

        private readonly double backgroundHeight;
        private readonly double backgroundWidth;
        private double[] enemyShipRowYCoordinates;
        private double playerPositionOnFire;
        private double initialEnemyShipOffsetX;

        private Canvas background;

        private PlayerShip playerShip;
        private Fleet enemyFleet;

        private List<Shield> shieldList;
        private List<Bullet> activeEnemyBullets;
        private List<Bullet> activePlayerBullets;

        private DispatcherTimer timer;
        private readonly TimeSpan gameTickInterval = new TimeSpan(0, 0, 0, 0, TickInterval);
        private DispatcherTimer bonusShipTimer;
        private readonly TimeSpan bonusTickInterval = new TimeSpan(0, 0, 0, 1, 0);

        private EnemyShipBonusShip bonusShip;

        private bool isBonusShipActive;

        private Random bonusRandom;

        private AudioManager audioManager;
        private CollisionManager collisionManager;

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the player's score.
        /// </summary>
        /// <value>
        ///     The player's score.
        /// </value>
        public int Score { get; private set; }

        /// <summary>
        ///     Gets or sets the player's lives.
        /// </summary>
        /// <value>
        ///     The player's lives
        /// </value>
        public int PlayerLives { get; private set; }

        /// <summary>
        ///     Gets the game level.
        /// </summary>
        /// <value>
        ///     The current game level.
        /// </value>
        public int GameLevel { get; private set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="GameManager" /> class.
        ///     Precondition: backgroundHeight > 0 AND backgroundWidth > 0
        /// </summary>
        /// <param name="backgroundHeight">The backgroundHeight of the game play window.</param>
        /// <param name="backgroundWidth">The backgroundWidth of the game play window.</param>
        public GameManager(double backgroundHeight, double backgroundWidth)
        {
            if (backgroundHeight <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(backgroundHeight));
            }

            if (backgroundWidth <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(backgroundWidth));
            }

            this.backgroundHeight = backgroundHeight;
            this.backgroundWidth = backgroundWidth;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Initializes the game placing player ship and enemy ship in the game.
        ///     Precondition: background != null
        ///     Postcondition: Game is initialized and ready for play.
        /// </summary>
        /// <param name="backgroundCanvas">The background canvas.</param>
        public void InitializeGame(Canvas backgroundCanvas)
        {
            if (backgroundCanvas == null)
            {
                throw new ArgumentNullException(nameof(backgroundCanvas));
            }

            this.background = backgroundCanvas;

            this.createPlayerShip();
            this.placePlayerShipNearBottomOfBackgroundCentered();

            this.enemyFleet = new Fleet();
            this.createEnemyShips();

            this.enemyShipRowYCoordinates = new[] {
                4*this.enemyFleet.EnemyShips[0].Height + Fleet.SpaceBetweenEnemyShips*3,
                3*this.enemyFleet.EnemyShips[0].Height + Fleet.SpaceBetweenEnemyShips*2,
                2*this.enemyFleet.EnemyShips[0].Height + Fleet.SpaceBetweenEnemyShips,
                this.enemyFleet.EnemyShips[0].Height
            };

            this.initialEnemyShipOffsetX = (this.backgroundWidth -
                                            this.enemyFleet.EnemyShips.Count*this.enemyFleet.EnemyShips[0].Width/3 -
                                            Fleet.SpaceBetweenEnemyShips*this.enemyFleet.EnemyShips.Count/3)/2;
            this.placeEnemyShips();
            this.createShields();

            this.activeEnemyBullets = new List<Bullet>();
            this.activePlayerBullets = new List<Bullet>();

            this.PlayerLives = 3;
            this.GameLevel = 1;
            this.difficultyModifier = 50;

            this.timer = new DispatcherTimer {Interval = this.gameTickInterval};
            this.timer.Tick += this.handleNormalGameplay;
            this.timer.Start();

            this.bonusShipTimer = new DispatcherTimer {Interval = this.bonusTickInterval};
            this.bonusShipTimer.Tick += this.createBonusShip;
            this.bonusShipTimer.Start();

            this.audioManager = new AudioManager();
            this.collisionManager = new CollisionManager();

            this.bonusRandom = new Random();
        }

        private void handleNormalGameplay(object sender, object o)
        {
            this.enemyFleet.MoveEnemyShipsOnTick(this.backgroundWidth);
            this.moveBulletsOnTick();
            this.handleEnemyFire();

            if (this.isBonusShipActive)
            {
                this.bonusShip.X += this.bonusShip.SpeedX;

                if (Math.Abs(this.bonusShip.X - this.backgroundWidth) < 0.1)
                {
                    this.isBonusShipActive = false;
                    this.background.Children.Remove(this.bonusShip.Sprite);
                    this.bonusShipTimer.Start();
                }
            }
        }

        private void createBonusShip(object sender, object e)
        {
            if (!this.isBonusShipActive && this.bonusRandom.Next(1, 50) <= this.bonusShipChance)
            {
                this.bonusShip = new EnemyShipBonusShip();
                this.bonusShip.X = this.bonusShip.Width;
                this.bonusShip.Y = this.bonusShip.Height/2;

                this.audioManager.PlaySfx("bonusShipSpawn.wav");

                this.background.Children.Add(this.bonusShip.Sprite);
                this.isBonusShipActive = true;
            }
        }

        private void createShields()
        {
            this.shieldList = new List<Shield> {
                new Shield {X = 0 + this.backgroundWidth/4 - 50, Y = 400},
                new Shield {X = 0 + this.backgroundWidth/4*2 - 50, Y = 400},
                new Shield {X = 0 + this.backgroundWidth/4*3 - 50, Y = 400}
            };

            foreach (var shield in this.shieldList)
            {
                this.background.Children.Add(shield.Sprite);
            }
        }

        private void createEnemyShips()
        {
            this.enemyFleet.CreateFleet();
            foreach (var enemyShip in this.enemyFleet.EnemyShips)
            {
                this.background.Children.Add(enemyShip.Sprite);
            }
        }

        private void createPlayerShip()
        {
            this.playerShip = ShipFactory.CreatePlayerShip();
            this.background.Children.Add(this.playerShip.Sprite);
        }

        private void placeEnemyShips()
        {
            var enemyShipXOffsets = new[] {
                this.initialEnemyShipOffsetX,
                this.initialEnemyShipOffsetX + this.enemyFleet.EnemyShips[0].Width +
                Fleet.SpaceBetweenEnemyShips,
                this.initialEnemyShipOffsetX +
                2*(this.enemyFleet.EnemyShips[0].Width + Fleet.SpaceBetweenEnemyShips),
                this.initialEnemyShipOffsetX +
                3*(this.enemyFleet.EnemyShips[0].Width + Fleet.SpaceBetweenEnemyShips)
            };
            foreach (var enemyShip in this.enemyFleet.EnemyShips)
            {
                if (enemyShip is EnemyShipLevel4)
                {
                    enemyShipXOffsets[0] = setEnemyShipCoordinates(enemyShip, enemyShipXOffsets[0],
                        this.enemyShipRowYCoordinates[3]);
                }
                else if (enemyShip is EnemyShipLevel3)
                {
                    enemyShipXOffsets[1] = setEnemyShipCoordinates(enemyShip, enemyShipXOffsets[1],
                        this.enemyShipRowYCoordinates[2]);
                }
                else if (enemyShip is EnemyShipLevel2)
                {
                    enemyShipXOffsets[2] = setEnemyShipCoordinates(enemyShip, enemyShipXOffsets[2],
                        this.enemyShipRowYCoordinates[1]);
                }
                else if (enemyShip is EnemyShipLevel1)
                {
                    enemyShipXOffsets[3] = setEnemyShipCoordinates(enemyShip, enemyShipXOffsets[3],
                        this.enemyShipRowYCoordinates[0]);
                }
            }
        }

        private static double setEnemyShipCoordinates(EnemyShip enemyShip, double shipXOffset, double enemyShipRowY)
        {
            enemyShip.Y = enemyShipRowY + BonusShipSpaceYOffset;
            enemyShip.X = Fleet.SpaceBetweenEnemyShips + shipXOffset;
            shipXOffset += enemyShip.Width + Fleet.SpaceBetweenEnemyShips;
            return shipXOffset;
        }

        private void placePlayerShipNearBottomOfBackgroundCentered()
        {
            this.playerShip.X = this.backgroundWidth/2 - this.playerShip.Width/2.0;
            this.playerShip.Y = this.backgroundHeight - this.playerShip.Height - PlayerShipBottomOffset;
        }

        /// <summary>
        ///     Moves the player ship to the left.
        ///     Precondition: player ship must not be at the left boundary of the page.
        ///     Postcondition: The player ship has moved left.
        /// </summary>
        public void MovePlayerShipLeft()
        {
            if (!this.playerShipIsAtLeftCanvasBoundary())
            {
                this.playerShip.MoveLeft();
            }
        }

        /// <summary>
        ///     Moves the player ship to the right.
        ///     Precondition: player ship must not be at the right boundary of the page.
        ///     Postcondition: The player ship has moved right.
        /// </summary>
        public void MovePlayerShipRight()
        {
            if (!this.playerShipIsAtRightCanvasBoundary())
            {
                this.playerShip.MoveRight();
            }
        }

        private bool playerShipIsAtLeftCanvasBoundary()
        {
            return this.playerShip.X <= this.playerShip.SpeedX;
        }

        private bool playerShipIsAtRightCanvasBoundary()
        {
            return this.playerShip.X + this.playerShip.Width >= this.backgroundWidth - this.playerShip.SpeedX;
        }

        /// <summary>
        ///     Fires the player bullet.
        ///     Precondition: Player cannot have another bullet active.
        ///     Postcondition: The PlayerShip fires a bullet.
        /// </summary>
        public void FirePlayerBullet()
        {
            if (this.activePlayerBullets.Count >= this.maxPlayerBullets)
            {
                return;
            }
            if (this.activePlayerBullets.Count == 0)
            {
                this.addFirstPlayerBullet();
            }
            else
            {
                this.addMultipleBulletsWithSpacing();
            }
        }

        private void addMultipleBulletsWithSpacing()
        {
            var playerBullet = new Bullet {
                X = this.playerShip.X + this.playerShip.Width/2,
                Y = this.playerShip.Y,
                IsActive = true
            };
            foreach (var bullet in this.activePlayerBullets)
            {
                if (playerBullet.Y >= bullet.Y && playerBullet.Y <= bullet.Y + bullet.Height + BulletSpacingDelay)
                {
                    return;
                }
            }
            this.activePlayerBullets.Add(playerBullet);
            this.background.Children.Add(playerBullet.Sprite);
            this.audioManager.PlaySfx("shoot.wav");
        }

        private void addFirstPlayerBullet()
        {
            var playerBullet = new Bullet {
                X = this.playerShip.X + this.playerShip.Width/2,
                Y = this.playerShip.Y,
                IsActive = true
            };
            this.activePlayerBullets.Add(playerBullet);
            this.background.Children.Add(playerBullet.Sprite);
            this.audioManager.PlaySfx("shoot.wav");
        }

        private void handleEnemyFire()
        {
            foreach (var enemyShip in this.enemyFleet.EnemyShips)
            {
                if (this.enemyShipIsReadyToFire(enemyShip))
                {
                    this.fireEnemyBullet(enemyShip);
                }
            }

            if (this.isBonusShipActive)
            {
                if (this.enemyShipIsReadyToFire(this.bonusShip))
                {
                    this.fireEnemyBullet(this.bonusShip);
                }
            }
        }

        private bool enemyShipIsReadyToFire(EnemyShip enemyShip)
        {
            return enemyShip.CanFire &&
                   this.enemyFleet.RandomEnemyFire.Next(Fleet.EnemyMinHitRange, Fleet.MaxEnemyHitRange) ==
                   this.enemyFleet.EnemyHitChance;
        }

        private void fireEnemyBullet(EnemyShip enemyShip)
        {
            var enemyBullet = new Bullet {
                Y = enemyShip.Y,
                X = enemyShip.X + enemyShip.Width/2,
                IsActive = true
            };

            this.playerPositionOnFire = this.playerShip.X;
            this.ticksToPlayerShip = ((int) this.playerShip.Y - (int) enemyBullet.Y)/enemyBullet.SpeedY;

            enemyBullet.XSpeedBasedOnPlayerPosition = ((int) this.playerPositionOnFire + (int) this.playerShip.Width/2 -
                                                       (int) enemyBullet.X)/this.ticksToPlayerShip;

            this.activeEnemyBullets.Add(enemyBullet);
            this.background.Children.Add(enemyBullet.Sprite);
            this.audioManager.PlaySfx("enemyShoot.wav");
        }

        private void moveBulletsOnTick()
        {
            this.movePlayerBullets();
            this.moveEnemyBullets();
            this.removeBulletsPastBoundary();
        }

        private void movePlayerBullets()
        {
            foreach (var bullet in this.activePlayerBullets.ToArray())
            {
                if (bullet.IsActive)
                {
                    bullet.MoveUp();
                    this.checkEnemiesForCollisionsWithPlayerBullet(bullet);
                    this.checkShieldForCollisionWithBullet(bullet);
                }
            }
        }

        private void moveEnemyBullets()
        {
            if (this.activeEnemyBullets.Count > 0)
            {
                foreach (var bullet in this.activeEnemyBullets.ToArray())
                {
                    this.checkPlayerForCollisionWithEnemyBullet(bullet);
                    this.checkShieldForCollisionWithBullet(bullet);

                    bullet.X += bullet.XSpeedBasedOnPlayerPosition;
                    bullet.MoveDown();
                }
            }
        }

        private void checkShieldForCollisionWithBullet(Bullet bullet)
        {
            foreach (var shield in this.shieldList.ToArray())
            {
                if (this.collisionManager.FindCollisionBetween(shield, bullet))
                {
                    this.audioManager.PlaySfx("playerDestroyed.wav");
                    this.removeBulletFromPlay(bullet);
                    shield.ShieldHp--;

                    if (shield.ShieldHp == 4)
                    {
                        shield.ChangeColor(new SolidColorBrush(Colors.YellowGreen));
                    }
                    else if (shield.ShieldHp == 3)
                    {
                        shield.ChangeColor(new SolidColorBrush(Colors.Yellow));
                    }
                    else if (shield.ShieldHp == 2)
                    {
                        shield.ChangeColor(new SolidColorBrush(Colors.Orange));
                    }
                    else if (shield.ShieldHp == 1)
                    {
                        shield.ChangeColor(new SolidColorBrush(Colors.Red));
                    }
                    else
                    {
                        this.background.Children.Remove(shield.Sprite);
                        this.shieldList.Remove(shield);
                    }
                }
            }
        }

        private void checkEnemiesForCollisionsWithPlayerBullet(Bullet playerBullet)
        {
            foreach (var enemyShip in this.enemyFleet.EnemyShips)
            {
                if (this.collisionManager.FindCollisionBetween(enemyShip, playerBullet))
                {
                    this.destroyEnemyShip(playerBullet, enemyShip);
                    break;
                }
            }
            if (this.isBonusShipActive)
            {
                if (this.collisionManager.FindCollisionBetween(this.bonusShip, playerBullet))
                {
                    this.destroyBonushShip(playerBullet);
                    this.maxPlayerBullets = 10;
                }
            }
        }

        private void checkPlayerForCollisionWithEnemyBullet(Bullet enemyBullet)
        {
            if (this.collisionManager.FindCollisionBetween(this.playerShip, enemyBullet) && !this.playerShip.IsDestroyed)
            {
                this.audioManager.PlaySfx("playerDestroyed.wav");
                this.playerShip.X = this.backgroundWidth/2 - this.playerShip.Width/2;
                this.removeBulletFromPlay(enemyBullet);
                this.PlayerLives--;

                if (this.PlayerLives == 0)
                {
                    this.timer.Stop();
                    this.background.Children.Remove(this.playerShip.Sprite);
                }
            }
        }

        private void removeBulletFromPlay(Bullet bullet)
        {
            if (this.activePlayerBullets.Contains(bullet))
            {
                this.activePlayerBullets.Remove(bullet);
            }
            else if (this.activeEnemyBullets.Contains(bullet))
            {
                this.activeEnemyBullets.Remove(bullet);
            }
            this.background.Children.Remove(bullet.Sprite);
            bullet.IsActive = false;
        }

        private void removeAllBulletsFromPlay()
        {
            foreach (var bullet in this.activePlayerBullets.ToArray())
            {
                this.activePlayerBullets.Remove(bullet);
                this.background.Children.Remove(bullet.Sprite);
            }

            foreach (var bullet in this.activeEnemyBullets.ToArray())
            {
                this.activeEnemyBullets.Remove(bullet);
                this.background.Children.Remove(bullet.Sprite);
            }
        }

        private void removeBulletsPastBoundary()
        {
            foreach (var bullet in this.activeEnemyBullets)
            {
                if (bullet.Y > this.backgroundHeight)
                {
                    this.activeEnemyBullets.Remove(bullet);
                    this.background.Children.Remove(bullet.Sprite);
                    break;
                }
            }

            foreach (var bullet in this.activePlayerBullets)
            {
                if (bullet.Y < 0)
                {
                    this.activePlayerBullets.Remove(bullet);
                    this.background.Children.Remove(bullet.Sprite);
                    break;
                }
            }
        }

        private void updatePlayerScore(int enemyShipPointValue)
        {
            this.Score += enemyShipPointValue;
        }

        private void destroyBonushShip(Bullet playerBullet)
        {
            this.audioManager.PlaySfx("powerUp.wav");
            this.removeBulletFromPlay(playerBullet);
            this.background.Children.Remove(this.bonusShip.Sprite);
            this.updatePlayerScore(this.bonusShip.PointValue);
            this.isBonusShipActive = false;
            this.bonusShipTimer.Start();
        }

        private void destroyEnemyShip(Bullet playerBullet, EnemyShip enemyShip)
        {
            this.audioManager.PlaySfx("enemyDestroyed.wav");
            this.removeBulletFromPlay(playerBullet);
            this.background.Children.Remove(enemyShip.Sprite);
            this.enemyFleet.EnemyShips.Remove(enemyShip);
            this.updatePlayerScore(enemyShip.PointValue);
            this.handleEndOfLevel();
        }

        private void handleEndOfLevel()
        {
            if (this.enemyFleet.EnemyShips.Count == 0)
            {
                if (this.GameLevel == 3)
                {
                    this.timer.Stop();
                }
                else
                {
                    this.moveToNextLevel();
                }
            }
        }

        private void moveToNextLevel()
        {
            this.GameLevel++;

            foreach (var shield in this.shieldList)
            {
                this.background.Children.Remove(shield.Sprite);
            }

            this.removeAllBulletsFromPlay();

            this.createShields();
            this.createEnemyShips();
            this.placeEnemyShips();
            this.modifyEnemyFlightPattern();

            Fleet.MaxEnemyHitRange -= this.difficultyModifier;

            this.maxPlayerBullets = 3;

            if (this.isBonusShipActive)
            {
                this.isBonusShipActive = false;
                this.background.Children.Remove(this.bonusShip.Sprite);
            }
        }

        private void modifyEnemyFlightPattern()
        {
            switch (this.GameLevel)
            {
                case 2:
                    this.enemyFleet.SetShipSpeeds(8, typeof(EnemyShipLevel1));
                    this.enemyFleet.SetShipSpeeds(8, typeof(EnemyShipLevel2));
                    break;
                case 3:
                    this.enemyFleet.SetShipSpeeds(10, typeof(EnemyShipLevel1));
                    this.enemyFleet.SetShipSpeeds(10, typeof(EnemyShipLevel2));
                    this.enemyFleet.SetShipSpeeds(6, typeof(EnemyShipLevel3));
                    this.enemyFleet.SetShipSpeeds(6, typeof(EnemyShipLevel4));
                    break;
            }
        }

        /// <summary>
        ///     Determines whether the game is over.
        ///     Precondition: None.
        /// </summary>
        /// <returns>
        ///     <c>true</c> if the game is over; otherwise, <c>false</c>.
        /// </returns>
        public bool IsGameOver()
        {
            return this.GameLevel == 3 && this.enemyFleet.EnemyShips.Count == 0 || this.PlayerLives == 0;
        }

        #endregion
    }
}