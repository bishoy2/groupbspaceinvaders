﻿using SpaceInvaders.View.Sprites;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Manages a level 1 enemy ship.
    /// </summary>
    /// <seealso cref="SpaceInvaders.Model.GameObject" />
    public class EnemyShipBonusShip : EnemyShip
    {
        #region Data members

        private const int SpeedXDirection = 5;
        private const int SpeedYDirection = 0;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="EnemyShipBonusShip" /> class.
        ///     The bonus ship is a Level 4 ship that appears every thirty seconds above the fleet
        ///     to scroll across the screen and fire.
        /// </summary>
        public EnemyShipBonusShip()
        {
            Sprite = new EnemyShipLevel4Sprite();
            SetSpeed(SpeedXDirection, SpeedYDirection);
            PointValue = 10;
            CanFire = true;
        }

        #endregion
    }
}