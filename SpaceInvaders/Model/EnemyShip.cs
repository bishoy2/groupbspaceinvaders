﻿namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Defines the basics of an Enemy Ship.
    /// </summary>
    public abstract class EnemyShip : GameObject
    {
        #region Properties

        /// <summary>
        ///     Gets or sets the point value.
        /// </summary>
        /// <value>
        ///     The point value.
        /// </value>
        public int PointValue { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether this ship can fire bullets.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance can fire; otherwise, <c>false</c>.
        /// </value>
        public bool CanFire { get; set; }

        #endregion
    }
}