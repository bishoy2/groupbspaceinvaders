﻿using Windows.UI.Xaml.Media;
using SpaceInvaders.View.Sprites;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Manages the shield.
    /// </summary>
    /// <seealso cref="SpaceInvaders.Model.GameObject" />
    public class Shield : GameObject
    {
        #region Data members

        private readonly ShieldSprite shieldSprite;

        #endregion

        #region Properties

        /// <summary>
        ///     Number of times a shield can be hit by bullets before disappearing
        /// </summary>
        /// <value>
        ///     The shield hp.
        /// </value>
        public int ShieldHp { get; set; } = 5;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Shield" /> class.
        /// </summary>
        public Shield()
        {
            this.shieldSprite = new ShieldSprite();
            Sprite = this.shieldSprite;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Changes the color of the shield based on a SolidColorBrush passed in
        /// </summary>
        /// <param name="fill">The fill.</param>
        public void ChangeColor(SolidColorBrush fill)
        {
            this.shieldSprite.ChangeColor(fill);
        }

        #endregion
    }
}