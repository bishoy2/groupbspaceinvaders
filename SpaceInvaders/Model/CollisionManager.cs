﻿namespace SpaceInvaders.Model
{
    internal class CollisionManager
    {
        #region Methods

        public bool FindCollisionBetween(GameObject gameObject, Bullet bullet)
        {
            if (bullet.IsActive && ShipsIntersect(gameObject, bullet))
            {
                return true;
            }
            return false;
        }

        public static bool ShipsIntersect(GameObject gameObject, Bullet bullet)
        {
            return bullet.X >= gameObject.X && bullet.X <= gameObject.X + gameObject.Width && bullet.Y >= gameObject.Y &&
                   bullet.Y <= gameObject.Y + gameObject.Height;
        }

        #endregion
    }
}