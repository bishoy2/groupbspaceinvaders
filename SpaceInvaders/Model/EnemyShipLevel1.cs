﻿using SpaceInvaders.View.Sprites;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Manages a level 1 enemy ship.
    /// </summary>
    /// <seealso cref="SpaceInvaders.Model.GameObject" />
    public class EnemyShipLevel1 : EnemyShip
    {
        #region Data members

        private const int SpeedXDirection = 2;
        private const int SpeedYDirection = 0;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="EnemyShipLevel1" /> class.
        /// </summary>
        public EnemyShipLevel1()
        {
            Sprite = new EnemyShipLevel1Sprite();
            SetSpeed(SpeedXDirection, SpeedYDirection);
            PointValue = 1;
            CanFire = false;
        }

        #endregion
    }
}