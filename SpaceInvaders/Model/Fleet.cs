﻿using System;
using System.Collections.Generic;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Models a collection of enemy ships.
    /// </summary>
    public class Fleet
    {
        #region Data members

        /// <summary>
        ///     The enemy hit range
        /// </summary>
        public static int MaxEnemyHitRange = 300;

        /// <summary>
        ///     The enemy minimum hit range
        /// </summary>
        public const int EnemyMinHitRange = 0;

        /// <summary>
        ///     The space between enemy ships in the fleet.
        /// </summary>
        public const double SpaceBetweenEnemyShips = 20;

        /// <summary>
        ///     The value that triggers a fire event if the random number generator returns it on a tick
        /// </summary>
        public int EnemyHitChance = 150;

        /// <summary>
        ///     The random value that determines enemy fire.
        /// </summary>
        public Random RandomEnemyFire;

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the enemy ships.
        /// </summary>
        /// <value>
        ///     The enemy ships.
        /// </value>
        public List<EnemyShip> EnemyShips { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Fleet" /> class.
        /// </summary>
        public Fleet()
        {
            this.RandomEnemyFire = new Random();
            this.movingLeft = true;
            this.movingRight = false;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Creates the fleet.
        /// </summary>
        public void CreateFleet()
        {
            this.EnemyShips = new List<EnemyShip>();
            for (var i = 0; i < NumberOfLevelOneShips; i++)
            {
                this.EnemyShips.Add(ShipFactory.CreateEnemyShip(typeof(EnemyShipLevel1)));
            }
            for (var i = 0; i < NumberOfLevelTwoShips; i++)
            {
                this.EnemyShips.Add(ShipFactory.CreateEnemyShip(typeof(EnemyShipLevel2)));
            }
            for (var i = 0; i < NumberOfLevelThreeShips; i++)
            {
                this.EnemyShips.Add(ShipFactory.CreateEnemyShip(typeof(EnemyShipLevel3)));
            }
            for (var i = 0; i < NumberOfLevelFourShips; i++)
            {
                this.EnemyShips.Add(ShipFactory.CreateEnemyShip(typeof(EnemyShipLevel4)));
            }
        }

        private void moveFleetLeft()
        {
            foreach (var enemyShip in this.EnemyShips)
            {
                enemyShip.MoveLeft();
            }
            if (this.enemyShipsAreAtLeftBoundary())
            {
                this.movingLeft = false;
                this.movingRight = true;
            }
        }

        private void moveFleetRight(double backgroundWidth)
        {
            foreach (var enemyShip in this.EnemyShips)
            {
                enemyShip.MoveRight();
            }
            if (this.enemyShipsAreAtRightBoundary(backgroundWidth))
            {
                this.movingLeft = true;
                this.movingRight = false;
            }
        }

        /// <summary>
        ///     Sets the ship speeds.
        /// </summary>
        /// <param name="speed">The speed.</param>
        /// <param name="shipType"></param>
        public void SetShipSpeeds(int speed, Type shipType)
        {
            foreach (var enemyShip in this.EnemyShips)
            {
                if (enemyShip.GetType() == shipType)
                {
                    enemyShip.SetSpeed(speed, 0);
                }
            }
        }

        private bool enemyShipsAreAtLeftBoundary()
        {
            foreach (var enemyShip in this.EnemyShips)
            {
                if (enemyShip.X <= enemyShip.SpeedX)
                {
                    return true;
                }
            }
            return false;
        }

        private bool enemyShipsAreAtRightBoundary(double backgroundWidth)
        {
            foreach (var enemyShip in this.EnemyShips)
            {
                if (enemyShip.X + enemyShip.Width >= backgroundWidth - enemyShip.SpeedX)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        ///     Moves the enemy ships on tick.
        /// </summary>
        /// <param name="backgroundWidth">Width of the background.</param>
        public void MoveEnemyShipsOnTick(double backgroundWidth)
        {
            if (this.movingLeft)
            {
                this.moveFleetLeft();
            }
            else if (this.movingRight)
            {
                this.moveFleetRight(backgroundWidth);
            }
        }

        #endregion

        #region Data Members

        private const int NumberOfLevelOneShips = 2;
        private const int NumberOfLevelTwoShips = 4;
        private const int NumberOfLevelThreeShips = 6;
        private const int NumberOfLevelFourShips = 8;
        private bool movingLeft;
        private bool movingRight;

        #endregion
    }
}