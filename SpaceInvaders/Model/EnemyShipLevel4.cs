﻿using SpaceInvaders.View.Sprites;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Manages a level 4 enemy ship.
    /// </summary>
    /// <seealso cref="SpaceInvaders.Model.GameObject" />
    public class EnemyShipLevel4 : EnemyShip
    {
        #region Data members

        private const int SpeedXDirection = 2;
        private const int SpeedYDirection = 0;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="EnemyShipLevel4" /> class.
        /// </summary>
        public EnemyShipLevel4()
        {
            Sprite = new EnemyShipLevel4Sprite();
            SetSpeed(SpeedXDirection, SpeedYDirection);
            PointValue = 4;
            CanFire = true;
        }

        #endregion
    }
}