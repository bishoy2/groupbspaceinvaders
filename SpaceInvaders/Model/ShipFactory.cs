﻿using System;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     A factory that produces ships for the model classes.
    /// </summary>
    public static class ShipFactory
    {
        #region Methods

        /// <summary>
        ///     Creates the player ship.
        /// </summary>
        /// <returns></returns>
        public static PlayerShip CreatePlayerShip()
        {
            return (PlayerShip) Activator.CreateInstance(typeof(PlayerShip));
        }

        /// <summary>
        ///     Creates the enemy ship.
        /// </summary>
        /// <param name="enemyShipType">Type of Enemy Ship.</param>
        /// <returns></returns>
        public static EnemyShip CreateEnemyShip(Type enemyShipType)
        {
            return Activator.CreateInstance(enemyShipType) as EnemyShip;
        }

        #endregion
    }
}