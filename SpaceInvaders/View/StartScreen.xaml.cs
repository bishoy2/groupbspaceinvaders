﻿using System;
using System.IO;
using Windows.Foundation;
using Windows.Storage;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The Content Dialog item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SpaceInvaders.View
{
    /// <summary>
    ///     Contains the functionality for the start screen.
    /// </summary>
    /// <seealso cref="Windows.UI.Xaml.Controls.ContentDialog" />
    public sealed partial class StartScreen
    {
        #region Data members

        private readonly MainPage pageNavigatingFrom;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="StartScreen" /> class.
        /// </summary>
        public StartScreen(MainPage page)
        {
            this.InitializeComponent();
            this.pageNavigatingFrom = page;
            Window.Current.CoreWindow.KeyDown += this.coreWindowOnKeyDown;
        }

        #endregion

        #region Methods

        private void coreWindowOnKeyDown(CoreWindow sender, KeyEventArgs args)
        {
            if (args.VirtualKey == VirtualKey.D)
            {
                this.devModeButton.Visibility = Visibility.Visible;
            }
        }

        private void ContentDialog_DisplayScoreboard(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            this.pageNavigatingFrom.Frame.Navigate(typeof(Scoreboard));
            ApplicationView.GetForCurrentView().TryResizeView(new Size {Width = 840, Height = 680});
        }

        private async void ContentDialog_ResetScoreboard(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            args.Cancel = true;
            var storageFolder = ApplicationData.Current.LocalFolder;
            try
            {
                var file = await storageFolder.GetFileAsync("ScoreboardData.csv");
                await file.DeleteAsync();
            }
            catch (FileNotFoundException)
            {
                var noFileDialog = new MessageDialog("No scoreboard data found.");
                await noFileDialog.ShowAsync();
                return;
            }
            var notifyDialog = new MessageDialog("Scoreboard reset.");
            await notifyDialog.ShowAsync();
        }

        private void StartGame_OnClick(object sender, RoutedEventArgs e)
        {
            Hide();
            this.pageNavigatingFrom.StartGameAndInitialize();
        }

        private async void DevModeButton_OnClick(object sender, RoutedEventArgs e)
        {
            Hide();
            var newEntry = new NewScoreboardEntry(1000, 6);
            await newEntry.ShowAsync();
            this.pageNavigatingFrom.Frame.Navigate(typeof(Scoreboard));
        }

        #endregion
    }
}