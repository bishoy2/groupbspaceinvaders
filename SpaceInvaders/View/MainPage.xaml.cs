﻿using System;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using SpaceInvaders.Model;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace SpaceInvaders.View
{
    /// <summary>
    ///     An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage
    {
        #region Data members

        private const double ApplicationHeight = 680;
        private const double ApplicationWidth = 840;

        private GameManager gameManager;
        private readonly StartScreen startScreen;

        private DispatcherTimer timer;
        private readonly TimeSpan gameTickInterval = new TimeSpan(0, 0, 0, 0, 1);

        private bool leftPressed;
        private bool rightPressed;
        private bool spacePressed;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="MainPage" /> class.
        /// </summary>
        public MainPage()
        {
            this.InitializeComponent();

            ApplicationView.PreferredLaunchViewSize = new Size {Width = ApplicationWidth, Height = ApplicationHeight};
            ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.PreferredLaunchViewSize;
            ApplicationView.GetForCurrentView().SetPreferredMinSize(new Size(ApplicationWidth, ApplicationHeight));

            this.startScreen = new StartScreen(this);
            this.showStartMenu();
        }

        #endregion

        #region Methods

        private async void showStartMenu()
        {
            await this.startScreen.ShowAsync();
        }

        /// <summary>
        ///     Starts and initializes the game.
        /// </summary>
        public void StartGameAndInitialize()
        {
            this.gameManager = new GameManager(ApplicationHeight, ApplicationWidth);

            Window.Current.CoreWindow.KeyDown += this.coreWindowOnKeyDown;
            Window.Current.CoreWindow.KeyUp += this.coreWindowOnKeyUp;

            this.leftPressed = false;
            this.rightPressed = false;
            this.spacePressed = false;

            this.scoreTextBlock.Visibility = Visibility.Visible;
            this.livesTextBlock.Visibility = Visibility.Visible;
            this.levelTextBlock.Visibility = Visibility.Visible;

            this.gameManager.InitializeGame(this.theCanvas);
            this.timer = new DispatcherTimer {Interval = this.gameTickInterval};
            this.timer.Tick += this.timerOnTick;
            this.timer.Start();
        }

        private async void timerOnTick(object sender, object o)
        {
            if (this.gameManager.IsGameOver())
            {
                this.timer.Stop();
                foreach (var child in this.theCanvas.Children)
                {
                    child.Visibility = Visibility.Collapsed;
                }
                await this.showGameOverDialog();
            }
            this.handleKeyPresses();
            this.levelTextBlock.Text = "Level: " + this.gameManager.GameLevel;
            this.scoreTextBlock.Text = "Score: " + this.gameManager.Score;
            this.livesTextBlock.Text = "Lives: " + this.gameManager.PlayerLives;
        }

        private async Task showGameOverDialog()
        {
            string gameOverMessage;
            if (this.gameManager.PlayerLives == 0)
            {
                gameOverMessage = "YOU LOSE.";
            }
            else
            {
                gameOverMessage = "YOU WIN!";
            }
            var gameOverDialog = new MessageDialog(gameOverMessage);
            gameOverDialog.Commands.Add(new UICommand("Next", this.moveToNextMenuAtEndGame));
            await gameOverDialog.ShowAsync();
        }

        private async void moveToNextMenuAtEndGame(IUICommand command)
        {
            var scoreboard = new Scoreboard();
            var lowestScore = scoreboard.FindLowestScore();
            if (this.gameManager.Score > lowestScore && scoreboard.Players.Count < 10)
            {
                var newEntry = new NewScoreboardEntry(this.gameManager.Score, this.gameManager.GameLevel);
                await newEntry.ShowAsync();
                ApplicationView.GetForCurrentView();
                Frame.Navigate(typeof(Scoreboard));
            }
            else
            {
                await this.startScreen.ShowAsync();
            }
        }

        private void handleKeyPresses()
        {
            if (this.leftPressed)
            {
                this.gameManager.MovePlayerShipLeft();
            }
            if (this.rightPressed)
            {
                this.gameManager.MovePlayerShipRight();
            }
            if (this.spacePressed)
            {
                this.gameManager.FirePlayerBullet();
            }
        }

        private void coreWindowOnKeyDown(CoreWindow sender, KeyEventArgs args)
        {
            switch (args.VirtualKey)
            {
                case VirtualKey.Left:
                    this.leftPressed = true;
                    break;
                case VirtualKey.Right:
                    this.rightPressed = true;
                    break;
                case VirtualKey.Space:
                    this.spacePressed = true;
                    break;
            }
        }

        private void coreWindowOnKeyUp(CoreWindow sender, KeyEventArgs args)
        {
            switch (args.VirtualKey)
            {
                case VirtualKey.Left:
                    this.leftPressed = false;
                    break;
                case VirtualKey.Right:
                    this.rightPressed = false;
                    break;
                case VirtualKey.Space:
                    this.spacePressed = false;
                    break;
            }
        }

        #endregion
    }
}