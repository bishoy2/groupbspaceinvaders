﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using SpaceInvaders.Model;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SpaceInvaders.View
{
    /// <summary>
    ///     The scoreboard for SpaceInvaders.
    /// </summary>
    public sealed partial class Scoreboard
    {
        #region Data members

        private TextBlock[] nameBlocks;
        private TextBlock[] scoreBlocks;
        private TextBlock[] levelReachedBlocks;

        #endregion

        #region Properties

        /// <summary>
        ///     The players in the xml file for the scoreboard.
        /// </summary>
        public List<Player> Players { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Scoreboard" /> class.
        /// </summary>
        public Scoreboard()
        {
            this.InitializeComponent();
            this.Players = new List<Player>();
            this.initializeTextBlockArrays();
        }

        #endregion

        #region Methods

        private void initializeTextBlockArrays()
        {
            this.initializeNameTextBlockArray();
            this.initializeScoreTextBlockArray();
            this.initializeLevelReachedTextBlockArray();
        }

        private void initializeNameTextBlockArray()
        {
            this.nameBlocks = new[] {
                this.nameRow1,
                this.nameRow2,
                this.nameRow3,
                this.nameRow4,
                this.nameRow5,
                this.nameRow6,
                this.nameRow7,
                this.nameRow8,
                this.nameRow9,
                this.nameRow10
            };
        }

        private void initializeScoreTextBlockArray()
        {
            this.scoreBlocks = new[] {
                this.scoreRow1,
                this.scoreRow2,
                this.scoreRow3,
                this.scoreRow4,
                this.scoreRow5,
                this.scoreRow6,
                this.scoreRow7,
                this.scoreRow8,
                this.scoreRow9,
                this.scoreRow10
            };
        }

        private void initializeLevelReachedTextBlockArray()
        {
            this.levelReachedBlocks = new[] {
                this.levelReachedRow1,
                this.levelReachedRow2,
                this.levelReachedRow3,
                this.levelReachedRow4,
                this.levelReachedRow5,
                this.levelReachedRow6,
                this.levelReachedRow7,
                this.levelReachedRow8,
                this.levelReachedRow9,
                this.levelReachedRow10
            };
        }

        private async Task populatePlayerListFromSaveFile()
        {
            var fileName = "ScoreboardData.csv";
            var storageFolder = ApplicationData.Current.LocalFolder;
            var file = await storageFolder.GetFileAsync(fileName);
            var buffer = await FileIO.ReadBufferAsync(file);
            using (var reader = DataReader.FromBuffer(buffer))
            {
                var text = reader.ReadString(buffer.Length);
                var fileLines = text.Split('\n');
                foreach (var line in fileLines)
                {
                    var playerInfo = line.Split(',');
                    if (playerInfo[0] != "")
                    {
                        var player = new Player(playerInfo[0], int.Parse(playerInfo[1]), int.Parse(playerInfo[2]));
                        this.Players.Add(player);
                    }
                }
            }
            this.Players = new List<Player>(this.Players.OrderBy(player => player.PlayerName));
        }

        /// <summary>
        ///     Finds the lowest score.
        /// </summary>
        public int FindLowestScore()
        {
            if (this.Players.Count == 0)
            {
                return 0;
            }
            return this.Players.Min(player => player.PlayerScore);
        }

        private void populateScoreboardFields()
        {
            for (var i = 0; i < this.Players.Count; i++)
            {
                this.nameBlocks[i].Text = this.Players[i].PlayerName;
                this.scoreBlocks[i].Text = this.Players[i].PlayerScore.ToString();
                this.levelReachedBlocks[i].Text = this.Players[i].LevelCompleted.ToString();
            }
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(MainPage));
        }

        private void sortByName_Click(object sender, RoutedEventArgs e)
        {
            this.Players = new List<Player>(this.Players.OrderBy(player => player.PlayerName));
            this.populateScoreboardFields();
        }

        private void sortByScore_Click(object sender, RoutedEventArgs e)
        {
            this.Players = new List<Player>(this.Players.OrderByDescending(player => player.PlayerScore));
            this.populateScoreboardFields();
        }

        private void sortByLevel_Click(object sender, RoutedEventArgs e)
        {
            this.Players = new List<Player>(this.Players.OrderByDescending(player => player.LevelCompleted));
            this.populateScoreboardFields();
        }

        private async void Scoreboard_OnLoaded(object sender, RoutedEventArgs e)
        {
            this.InitializeComponent();
            this.Players = new List<Player>();
            this.initializeTextBlockArrays();
            try
            {
                await this.populatePlayerListFromSaveFile();
            }
            catch (Exception)
            {
                // ignored
            }

            this.populateScoreboardFields();
        }

        #endregion
    }
}