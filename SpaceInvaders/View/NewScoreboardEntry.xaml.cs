﻿using System;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The Content Dialog item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SpaceInvaders.View
{
    /// <summary>
    ///     Dialog for entering in your name for submission to the scoreboard. Writes to the scoreboard data file.
    /// </summary>
    /// <seealso cref="Windows.UI.Xaml.Controls.ContentDialog" />
    public sealed partial class NewScoreboardEntry
    {
        #region Data members

        private readonly int score;
        private readonly int levelReached;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="NewScoreboardEntry" /> class.
        /// </summary>
        public NewScoreboardEntry(int score, int levelReached)
        {
            this.InitializeComponent();
            this.score = score;
            this.levelReached = levelReached;
        }

        #endregion

        #region Methods

        private async void ContentDialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var name = this.nameBox.Text;
            if (name == "" || name.Length > 20)
            {
                this.warningMsg.Visibility = Visibility.Visible;
            }
            else
            {
                await this.addPlayerLineToFile();
            }
        }

        private async Task addPlayerLineToFile()
        {
            var storageFolder = ApplicationData.Current.LocalFolder;
            var file = await storageFolder.CreateFileAsync("ScoreboardData.csv", CreationCollisionOption.OpenIfExists);
            var output = this.nameBox.Text.Replace(",", "") + "," + this.score + "," + this.levelReached +
                         Environment.NewLine;
            await FileIO.AppendTextAsync(file, output);
        }

        private void ContentDialog_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            Hide();
        }

        #endregion
    }
}