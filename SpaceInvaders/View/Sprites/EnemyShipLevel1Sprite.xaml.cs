﻿// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SpaceInvaders.View.Sprites
{
    /// <summary>
    ///     Draws an Level 1 Enemy Ship sprite.
    /// </summary>
    public sealed partial class EnemyShipLevel1Sprite
    {
        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="EnemyShipLevel1Sprite" /> class.
        /// </summary>
        public EnemyShipLevel1Sprite()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}