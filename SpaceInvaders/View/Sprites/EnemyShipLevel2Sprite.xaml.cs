﻿using System;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SpaceInvaders.View.Sprites
{
    /// <summary>
    ///     Draws an Level 2 Enemy Ship sprite.
    /// </summary>
    public sealed partial class EnemyShipLevel2Sprite
    {
        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="EnemyShipLevel2Sprite" /> class.
        /// </summary>
        public EnemyShipLevel2Sprite()
        {
            this.InitializeComponent();
            this.leftEye.Fill = Yellow;
            this.rightEye.Fill = Yellow;
            var timer = new DispatcherTimer {Interval = this.gameTickInterval};
            timer.Tick += this.animateShipOnTick;
            timer.Start();
        }

        #endregion

        #region Methods

        private void animateShipOnTick(object sender, object o)
        {
            changeColorOfEyes(this.leftEye);
            changeColorOfEyes(this.rightEye);
        }

        private static void changeColorOfEyes(Shape shape)
        {
            if (shape.Fill == Yellow)
            {
                shape.Fill = Red;
            }
            else if (shape.Fill == Red)
            {
                shape.Fill = Yellow;
            }
        }

        #endregion

        #region Data Members 

        private readonly TimeSpan gameTickInterval = new TimeSpan(0, 0, 0, 0, 1);
        private static readonly SolidColorBrush Yellow = new SolidColorBrush(Colors.Yellow);
        private static readonly SolidColorBrush Red = new SolidColorBrush(Colors.Red);

        #endregion
    }
}