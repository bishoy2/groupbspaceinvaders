﻿// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SpaceInvaders.View.Sprites
{
    /// <summary>
    ///     Draws an Level 4 Enemy Ship sprite.
    /// </summary>
    public sealed partial class EnemyShipLevel4Sprite
    {
        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="EnemyShipLevel4Sprite" /> class.
        /// </summary>
        public EnemyShipLevel4Sprite()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}