﻿// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SpaceInvaders.View.Sprites
{
    /// <summary>
    ///     Draws an Level 3 Enemy Ship sprite.
    /// </summary>
    public sealed partial class EnemyShipLevel3Sprite
    {
        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="EnemyShipLevel3Sprite" /> class.
        /// </summary>
        public EnemyShipLevel3Sprite()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}