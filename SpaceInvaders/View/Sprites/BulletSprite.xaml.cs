﻿// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SpaceInvaders.View.Sprites
{
    /// <summary>
    ///     Draws a bullet.
    /// </summary>
    public sealed partial class BulletSprite
    {
        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="BulletSprite" /> class.
        /// </summary>
        public BulletSprite()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}